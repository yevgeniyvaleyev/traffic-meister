# Installation

Node.js is required for this project (Version 5.12.0 of Node.js is used
during development)

1) Execute ```npm install``` to install all needed packages.

2) Execute ```npm start``` to build, run watch and start the application,
which will be available here http://localhost:8001 (or check logs).
Execute ```npm run build``` just to generate bundle and ```npm run watch```
to build in watch mode.

# Frameworks

Angular 1.5.8 is used as main framework.
Also Lodash is used for some specific data manipulations.

# Reusable components

All components are done as separate modules which are collected
dynamically with Browserify (for js) and other
gulp utils (for css). Components consist of angular components and
controllers (if needed).
This application has only one component, it's a component of main page.
Decided not to create additional components because controls of the
application are quite straightforward and simple.

# Responsiveness

The application is responsive, css flexbox and media are used to
achieve this goal.
No css framework is used.
UI is properly responsive for all modern screen resolutions.

# Unit tests

Unit tests are done for all models, services and components.
Run ```npm test``` to start unit tests.

# JS Docs

Functionality uses descriptive names for methods and functions, and I
tried to extract logic as much as possible which makes it very readable.
So JS documentation is not needed for this application.

# Bundling

In this work Browserify is used for bundle compilation.
UI dependencies are loaded only with NPM, to avoid Bower.
Babel is used for ES6 support.
Sass and Autoprefixer are used for CSS compilation.
Eslint style checking is used during build. And other utilities.
Bundle files are stored in ```bundle``` folder.

# Styles

UI is not pixel perfect and is made as close as possible to provided UI
mock but with additional adjustments which I decided to add. Logo and
header are changed according to page with. Selection details are put to
right container which becomes bottom container when screen is narrow.
