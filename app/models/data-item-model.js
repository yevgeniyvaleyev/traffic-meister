import _ from 'lodash';

const REQUIRED_FIELDS = ['id', 'type', 'brand', 'colors', 'img'];

export default class DataItemModel {

  constructor (dataItem) {

    if (!this.isValidData(dataItem)) {
      throw new Error('Data item is not valid!');
    }

    this._data = dataItem;
  }

  get id () {
    return this._data.id;
  }

  get type () {
    return this._data.type;
  }

  get brand () {
    return this._data.brand;
  }

  get colors () {
    return this._data.colors;
  }

  get image () {
    return this._data.img;
  }

  isValidData (data) {
    return _.isObject(data) && REQUIRED_FIELDS.every((field) => !_.isUndefined(data[field]));
  }

}
