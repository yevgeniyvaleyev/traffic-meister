import _ from 'lodash';
import DataItemModel from './data-item-model';

export default class DataCollectionModel {

  constructor (data) {
    this.items = [];

    if (!_.isArray(data)) {
      throw new Error('Server data is not valid!');
    }

    data.forEach((dataItem) => {
      this.items.push(new DataItemModel(dataItem));
    });
  }

  getTypes () {
    return _.uniq(_.map(this.items, 'type'));
  }

  getItemsByType (type) {
    return _.filter(this.items, {type: type});
  }
}
