import DataCollectionModel from '../data-collection-model';

describe('Data Service', function () {

  let dataCollectionModel;
  let dataMock = {
    id: 1,
    type: 'type',
    brand: 'brand',
    colors: [],
    img: 'test.dev'
  };

  beforeEach(() => {
    dataCollectionModel = new DataCollectionModel([dataMock, dataMock]);
  });

  describe('initialisation', () => {
    it('should be properly initialised', () => {
      expect(dataCollectionModel).toBeDefined();
    });

    it('should have "items" array', () => {
      expect(dataCollectionModel.items instanceof Array).toEqual(true);
      expect(dataCollectionModel.items.length).toEqual(2);
    });

    it('should throw if data is empty', () => {
      expect(() => {
        new DataCollectionModel();
      }).toThrowError('Server data is not valid!');
    });

    it('should throw if data is not array', () => {
      expect(() => {
        new DataCollectionModel({});
        new DataCollectionModel(1);
        new DataCollectionModel('');
      }).toThrowError('Server data is not valid!');
    });
  });

  describe('getTypes', () => {
    it('should return proper list of unique types', () => {
      expect(dataCollectionModel.getTypes()).toEqual(['type']);
    });
  });

  describe('getItemsByType', () => {
    it('should return list of items by valid type', () => {
      expect(dataCollectionModel.getItemsByType('type').length).toEqual(2);
    });

    it('should return empty list of items by invalid type', () => {
      expect(dataCollectionModel.getItemsByType('fake').length).toEqual(0);
    });
  });

});
