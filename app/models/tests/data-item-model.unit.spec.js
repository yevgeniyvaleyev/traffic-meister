import DataItemModel from '../data-item-model';

describe('Data Service', function () {

  let dataItemModel;
  let dataMock = {
    id: 1,
    type: 'type',
    brand: 'brand',
    colors: [],
    img: 'test.dev'
  };

  beforeEach(() => {
    dataItemModel = new DataItemModel(dataMock);
  });

  describe('initialisation', () => {
    it('should be properly initialised', () => {
      expect(dataItemModel).toBeDefined();
    });

    it('should throw if data is empty', () => {
      expect(() => {
        new DataItemModel();
      }).toThrowError('Data item is not valid!');
    });

    it('should throw if data is invalid', () => {
      expect(() => {
        new DataItemModel({});
      }).toThrowError('Data item is not valid!');
    });
  });

  describe('isValidData', () => {
    it('should return false for empty data', () => {
      expect(dataItemModel.isValidData()).toEqual(false);
    });

    it('should return false for invalid data', () => {
      expect(dataItemModel.isValidData({})).toEqual(false);
      expect(dataItemModel.isValidData(1)).toEqual(false);
      expect(dataItemModel.isValidData('test')).toEqual(false);
    });

    it('should return true for valid data', () => {
      expect(dataItemModel.isValidData(dataMock)).toEqual(true);
    });
  });

  describe('getter', () => {
    it('"id" should return proper value', () => {
      expect(dataItemModel.id).toEqual(dataMock.id);
    });

    it('"type" should return proper value', () => {
      expect(dataItemModel.type).toEqual(dataMock.type);
    });

    it('"brand" should return proper value', () => {
      expect(dataItemModel.brand).toEqual(dataMock.brand);
    });

    it('"colors" should return proper value', () => {
      expect(dataItemModel.colors).toEqual(dataMock.colors);
    });

    it('"image" should return proper value', () => {
      expect(dataItemModel.image).toEqual(dataMock.img);
    });
  });
});
