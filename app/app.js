import angular from 'angular';

import services from './services';
import pages from './pages';

export default angular.module('app', [
  'templates',
  pages.name,
  services.name
]);
