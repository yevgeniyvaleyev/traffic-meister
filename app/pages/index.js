import angular from 'angular';
import {MainComponent} from './main/main-component';

export default angular.module('app.pages', [])
  .component('main', MainComponent);
