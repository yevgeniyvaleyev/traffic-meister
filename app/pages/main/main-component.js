const template = require('./main-template.html');
import {MainController} from './main-controller';

export const MainComponent = {
  controller: MainController,
  templateUrl: template
};
