import DataCollectionModel from '../../models/data-collection-model';

export class MainController {

  /*@ngInject*/
  constructor (dataService) {
    this.data = {};
    this.vehicleTypes = [];
    this.dataCollection = [];
    this.filteredItems = [];
    this.dataService = dataService;
  }

  $onInit () {
    this.getData();
  }

  getData () {
    this.dataService.getData().then((data) => {
      this.dataCollection = new DataCollectionModel(data);
      this.vehicleTypes = this.dataCollection.getTypes();
    });
  }

  onTypeSelect (type) {
    this.filteredItems = this.dataCollection.getItemsByType(type);
  }
}
