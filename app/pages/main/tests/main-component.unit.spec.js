import angular from 'angular';
import module from '../../index';

describe('Main component', function () {

  let controller;
  let $componentController;
  let $q;
  let $scope;
  let dataService;
  let promise;
  let dataMock = {
    id: 1,
    type: 'type',
    brand: 'brand',
    colors: [],
    img: 'test.dev'
  };

  beforeEach(angular.mock.module(module.name));

  beforeEach(angular.mock.inject((_$componentController_, _$injector_) => {
    $componentController = _$componentController_;
    $q = _$injector_.get('$q');
    $scope = _$injector_.get('$rootScope').$new();
  }));

  beforeEach(() => {
    promise = $q.resolve([dataMock]);
    dataService = jasmine.createSpyObj(['getData']);
    dataService.getData.and.returnValue(promise);
  });

  beforeEach(() => {
    controller = $componentController('main', {dataService: dataService});
  });

  describe('constructor', () => {
    it('should have "data" defined', () => {
      expect(controller.data).toBeDefined();
    });

    it('should have "vehicleTypes" defined', () => {
      expect(controller.vehicleTypes).toBeDefined();
    });

    it('should have "dataCollection" defined', () => {
      expect(controller.dataCollection).toBeDefined();
    });

    it('should have "filteredItems" defined', () => {
      expect(controller.filteredItems).toBeDefined();
    });

    it('should have "dataService" injected', () => {
      expect(controller.dataService).toBeDefined();
    });
  });

  it('should call getData method on init phase', () => {
    spyOn(controller, 'getData').and.callThrough();
    controller.$onInit();
    expect(controller.getData).toHaveBeenCalled();
  });

  describe('getData', () => {
    it('should get server data', (done) => {
      controller.getData();
      promise.then(() => {
        expect(controller.dataCollection.items.length).toEqual(1);
        expect(controller.vehicleTypes).toEqual(['type']);
        done();
      });
      $scope.$digest();
    });
  });

  describe('onTypeSelect', () => {
    it('should get items by type', () => {
      let itemsMock = [{test: 1}];
      let type = 'testType';

      controller.dataCollection = jasmine.createSpyObj(['getItemsByType']);
      controller.dataCollection.getItemsByType.and.returnValue(itemsMock);

      controller.onTypeSelect(type);

      expect(controller.filteredItems).toEqual(itemsMock);
      expect(controller.dataCollection.getItemsByType).toHaveBeenCalledWith(type);
    });
  });
});
