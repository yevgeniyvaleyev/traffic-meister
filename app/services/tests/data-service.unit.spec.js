import angular from 'angular';
import module from '../';
import DataService from '../data-service';

describe('Data Service', function () {

  let $q;
  let $scope;
  let service;
  let serverDataMock = {
    test: 1
  };

  beforeEach(angular.mock.module(module.name));

  beforeEach(angular.mock.inject((_$q_, _$rootScope_) => {
    $q = _$q_;
    $scope = _$rootScope_.$new();
    service = new DataService($q);
  }));

  describe('initialisation', () => {
    it('should have angular promise service', () => {
      expect(service.$q).toEqual($q);
    });

    it('should have dataService service', () => {
      expect(service.serverDataService).toBeDefined();
    });
  });

  describe('data retrieving', () => {
    it('should call fetchData from serverData', (done) => {
      let deferred = $q.defer();

      spyOn(service.$q, 'defer').and.returnValue(deferred);
      spyOn(service.serverDataService, 'fetchData').and.callFake(() => {
        deferred.resolve(serverDataMock);
      });

      service.getData().then((data) => {
        expect(data).toEqual(serverDataMock);
        done();
      });
      $scope.$digest();
    });
  });
});
