const serverDataService = require('../../service');

export default class DataService {

  /*@ngInject*/
  constructor ($q) {
    this.$q = $q;
    this.serverDataService = serverDataService;
  }

  getData () {
    let deferred = this.$q.defer();

    this.serverDataService.fetchData((otherData, data) => {
      deferred.resolve(data);
    });

    return deferred.promise;
  }
}
