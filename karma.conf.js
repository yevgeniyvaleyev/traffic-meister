module.exports = function (config) {

  config.set({
    basePath: '',

    frameworks: ['jasmine', 'browserify'],

    files: [
      './node_modules/phantomjs-polyfill/bind-polyfill.js',
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './app/**/*.js',
      './app/**/*.unit.spec.js'
    ],

    exclude: [],

    reporters: ['dots'],

    preprocessors: {
      './app/**/*.js': [ 'browserify' ]
    },

    browserify: {
      debug: false,
      transform: [
        'babelify',
        [
          'browserify-ng-html2js',
          { module: 'templates' }
        ]
      ]
    },

    cache: false,

    // web server port
    port: 9876,

    colors: true,

    //
    // Possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_ERROR,

    autoWatch: false,

    //
    // Available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    //
    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // How long will Karma wait for a message from a browser before disconnecting from it (in ms).
    browserNoActivityTimeout: 60000
  });
};

