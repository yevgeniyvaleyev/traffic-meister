'use strict';

const autoprefixer = require('autoprefixer');
const gulp = require('gulp');
const babelify = require('babelify');
const del = require('del');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const notify = require('gulp-notify');
const concat = require('gulp-concat');
const eslint = require('gulp-eslint');
const uglify = require('gulp-uglify');
const inject = require('gulp-inject');
const buffer = require('vinyl-buffer');
const gulpif = require('gulp-if');
const annotate = require('gulp-ng-annotate');
const flatten = require('gulp-flatten');
const sequence = require('gulp-sequence');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const ngHtml2Js = require('browserify-ng-html2js');
const KarmaServer = require('karma').Server;
const path = require('path');

const isProduction = false; // will change

let paths = {
		karma: './karma.conf.js',
		scss: [
			'./app/styles/*.scss',
			'./app/**/*.scss',
			'!./app/**/*-mixins.scss'
		],
		js: './app/**/*.js',
		sourceFile: './app/app.js',
		app: './app',
		index: './app/index.html',
		images: ['./app/**/*.jpg', './app/**/*.png', './app/**/*.jpeg', './app/**/*.gif'],
		fonts: ['./app/**/*.ttf', './app/**/*.otf'],
		templates: './app/**/*-template.html',
		bundle: './bundle',
		bundleFonts: './bundle/fonts',
		bundleImages: './bundle/images',
		bundleStyles: './bundle/styles',
		bundleTpl: ['./**/*.js', './**/vendor.css', './**/app.css'],
		bundleJSFile: './app.js',
		bundleCSSFile: './app.css',
		bundleVendorCSSFile: './vendor.css'
	},
	postcssProcessors = [
		autoprefixer({
			cascade: false,
			browsers: 'last 2 versions'
		})
	],
	browserifyConfig = {
		entries: paths.sourceFile,
		cache: {},
		packageCache: {},
		debug: true
	};

/**
 * Run the karma server
 * @param {boolean} singleRun
 * @param {boolean} done
 */
function karma (singleRun, done) {
  return new KarmaServer({
    configFile: path.resolve(__dirname, paths.karma),
    singleRun: singleRun
  }, done);
}

function executeBrowserify (watch) {

	function handleErrors() {
		let args = Array.prototype.slice.call(arguments);
		notify.onError({
			title: 'Browserify Compile Error',
			message: '<%= error.message %>'
		}, ...args);
		this.emit('end'); // Keep gulp from hanging on this task
	}

	function compile () {
		return b
			.transform(babelify, {presets: ['es2015']})
			.transform(ngHtml2Js({
				module: 'templates'
			}))
			.bundle()
			.on('error', handleErrors)
			.pipe(source(paths.bundleJSFile))
			.pipe(annotate())
			.pipe(buffer())
			.pipe(gulpif(isProduction, uglify()))
			.pipe(gulp.dest(paths.bundle));
	}

	let b = browserify(browserifyConfig);

	return compile();
}

gulp.task('lint', () => {
	gulp
		.src(paths.js)
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});

gulp.task('clean', () => del(['bundle']));

gulp.task('copy:images', () => {
	gulp
		.src(paths.images)
		.pipe(flatten())
		.pipe(gulp.dest(paths.bundleImages), {cwd: paths.bundle});
});

gulp.task('copy:fonts', () => {
	gulp
		.src(paths.fonts)
		.pipe(flatten())
		.pipe(gulp.dest(paths.bundleFonts), {cwd: paths.bundle});
});

gulp.task('compile:index', () =>
	gulp.src(paths.index)
		.pipe(
			inject(
				gulp.src(paths.bundleTpl, {cwd: paths.bundle}), 
				{relative: true, ignorePath: '.' + paths.bundle}
			)
		)
		.pipe(gulp.dest(paths.bundle))
);

gulp.task('compile:app:styles', () =>
	gulp.src(paths.scss)
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(concat(paths.bundleCSSFile))
		.pipe(sourcemaps.write())
		.pipe(postcss(postcssProcessors))
		.pipe(gulp.dest(paths.bundleStyles))
);

gulp.task('browser-sync', () => {
	browserSync.init({
		port: 8001,
		notify: false,
		open: true,
		server: {
			baseDir: ['./bundle'],
			livereload: true
		}
	});
});

gulp.task('unit', sequence(['karma']));
gulp.task('karma', done => karma(true, done).start());
gulp.task('browserify', () => executeBrowserify());
gulp.task('styles', ['compile:app:styles']);

gulp.task('watch', () => {
	gulp.watch(paths.scss, ['compile:app:styles']).on('change', browserSync.reload);
	gulp.watch([paths.js, paths.templates], ['browserify']).on('change', browserSync.reload);
});
gulp.task('build', sequence(
	'clean',
	['lint', 'browserify', 'styles', 'copy:images', 'copy:fonts'],
	'compile:index')
);
gulp.task('serve', sequence('build', 'watch', 'browser-sync'));
